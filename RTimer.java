package simon.reactiveextension;

import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class RTimer {
	public interface AtTimerInterface {
		public void updateLastClickedResetButton(Long clickTime);
		public void updateSliderValue(int sliderValue);
	}
	
	public RTimer(AtTimerInterface atti) {
		this.atti = atti;
	}
	AtTimerInterface atti;
	Label lblElapsedProgressbar = new Label("Elapsed time:");
	Label lblElapsedSeconds = new Label("0 s");
	Label lblDuration = new Label("Duration (s):");
	
	JProgressBar progressBar = new JProgressBar(0,100);
	JSlider slider = new JSlider(1,100);
	JButton reset = new JButton("Reset");
	public void addComponentsToPane(Container pane){
		progressBar.setStringPainted(true);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setPaintTrack(true);
		slider.setMinorTickSpacing(5);
		slider.setMajorTickSpacing(25);
		slider.setFont(new Font("Tahoma", Font.BOLD,12));
		//slider.setFont(new Font("Tahoma", Font.BOLD,12));
		pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		pane.add(lblElapsedProgressbar,c);
		c.gridx = 1;
		pane.add(progressBar,c);
		c.gridx = 0;
		c.gridy = 1;
		pane.add(lblElapsedSeconds,c);
		c.gridy = 2;
		pane.add(lblDuration,c);
		c.gridx = 1;
		pane.add(slider,c);
		c.gridy = 3;
		c.anchor = GridBagConstraints.CENTER;
		pane.add(reset,c);
		slider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				atti.updateSliderValue((Integer)((JSlider)e.getSource()).getValue());
				//atti.updateLastClickedResetButton(System.currentTimeMillis());
			}
		});
		reset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				atti.updateLastClickedResetButton(System.currentTimeMillis());
			}
		});
		
	}
	public void display(){
		JFrame frame = new JFrame("Timer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		addComponentsToPane(frame.getContentPane());
		frame.pack();
		frame.setVisible(true);
	}
	
	public Label getLblElapsedSeconds() {
		return lblElapsedSeconds;
	}
	
	public int getDifferenceSeconds(Long seconds1, Long seconds2){
		Long difference = (seconds1 - seconds2)/1000;
		return Math.round(difference);		
	}
	public JProgressBar getProgressBar() {
		return progressBar;
	}
	
	
}
