package simon.reactiveextension;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTextField;


public class Spreadsheet {
	public interface AtSpreadsheetInterface {
		public void parse(String expr);
	}
	
	AtSpreadsheetInterface atss;
	
	JFrame jfrm = new JFrame("Spreadsheet");
	String[] cellStrings = { "A1", "A2", "A3", "B1", "B2", "B3" };
	JComboBox<String> jcCellList = new JComboBox<String>(cellStrings);
	JTextField jtfExpression = new JTextField();
	JButton jbtnSubmit = new JButton("OK");
	
	JTextField jtfA1 = new JTextField();
	JTextField jtfA2 = new JTextField();
	JTextField jtfA3 = new JTextField();
	JTextField jtfB1 = new JTextField();
	JTextField jtfB2 = new JTextField();
	JTextField jtfB3 = new JTextField();
	
	public void display(){
		jtfA1.setBackground(Color.YELLOW);
		jtfA2.setBackground(Color.YELLOW);
		jtfA3.setBackground(Color.YELLOW);
		jtfB1.setBackground(Color.YELLOW);
		jtfB2.setBackground(Color.YELLOW);
		jtfB3.setBackground(Color.YELLOW);
		
		jbtnSubmit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				atss.parse((jcCellList.getSelectedItem().toString() + "=" +jtfExpression.getText()).toUpperCase().trim().replaceAll("\\s",""));
			}
		});
		
		GridLayout gl = new GridLayout(0,2);
		jfrm.setLayout(gl);
		jfrm.setSize(200,200);
		jfrm.add(jcCellList);
		jfrm.add(jtfExpression);
		jfrm.add(jtfA1);
		jfrm.add(jtfB1);
		jfrm.add(jtfA2);
		jfrm.add(jtfB2);
		jfrm.add(jtfA3);
		jfrm.add(jtfB3);
		jfrm.add(jbtnSubmit);
		jfrm.setVisible(true);
	}
	

	public Spreadsheet(AtSpreadsheetInterface atss) {
		this.atss = atss;
	}	
	public JFrame getJfrm() {
		return jfrm;
	}
	public JTextField getJtfA1() {
		return jtfA1;
	}
	public JTextField getJtfA2() {
		return jtfA2;
	}
	public JTextField getJtfA3() {
		return jtfA3;
	}
	public JTextField getJtfB1() {
		return jtfB1;
	}
	public JTextField getJtfB2() {
		return jtfB2;
	}
	public JTextField getJtfB3() {
		return jtfB3;
	}
}
